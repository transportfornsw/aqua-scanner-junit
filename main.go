package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/pkg/errors"
	"github.com/rodrigodiez/go-junit/types"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

var cliFile = kingpin.Arg("file", "File to load containing the Aqua JSON report").Required().String()

func main() {
	kingpin.Parse()

	report, err := loadAquaReport(*cliFile)
	if err != nil {
		panic(err)
	}

	junit, err := reportToJunit(report)
	if err != nil {
		panic(err)
	}

	output, err := xml.MarshalIndent(junit, "", "  ")
	if err != nil {
		panic(err)
	}

	fmt.Println(string(output[:]))
}

// Helper function to marshal the Aqua report.
func loadAquaReport(path string) (Report, error) {
	var report Report

	file, err := os.Open(path)
	if err != nil {
		return report, errors.Wrap(err, "failed to open file")
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return report, errors.Wrap(err, "failed to read file")
	}

	err = json.Unmarshal(data, &report)
	if err != nil {
		return report, errors.Wrap(err, "failed to unmarshal data")
	}

	return report, nil
}

// Helper function to convert the report to a junit format.
func reportToJunit(input Report) (types.Testsuites, error) {
	// Group by CVE first, this improves readability.
	// We don't want a single file per "testsuite" which relates to a single CVE.
	grouped := make(map[string][]ReportCVE)

	for _, cve := range input.CVEs {
		name := fmt.Sprintf("%s (%s)", cve.Name, cve.URL)
		grouped[name] = append(grouped[name], cve)
	}

	var (
		tests []*types.Testsuite

		// Junit requires a total number of tests, to reliabily achieve this we
		// count all the tests (cves) as we loop through.
		total int
	)

	for name, cves := range grouped {
		var cases []*types.Testcase

		for _, cve := range cves {
			cases = append(cases, &types.Testcase{
				Id:   cve.File,
				Name: cve.File,
				Time: 0.01,
				Failures: []*types.Failure{
					{
						Message: cve.File,
						Type:    cve.Severity,
						Text:    cve.Description,
					},
				},
			})
		}

		test := &types.Testsuite{
			Id:        name,
			Name:      name,
			Tests:     len(cases),
			Failures:  len(cases),
			Time:      0.01,
			Testcases: cases,
		}

		tests = append(tests, test)

		total = total + test.Tests
	}

	output := types.Testsuites{
		Time:       0.01,
		Testsuites: tests,
	}

	return output, nil
}
